Name:    youtube-viewer
Version: 3.11.1
Release: 1%{?dist}
Summary: Youtube Viewer
Vendor:  Trizen <trizen@protonmail.com>

License: Artistic License 2.0
URL:     https://github.com/trizen/%{name}
Source0: %{url}/archive/%{version}.tar.gz

BuildArch: noarch
BuildRequires: perl(Module::Build)
BuildRequires: perl(JSON)
BuildRequires: perl(Test::More)
BuildRequires: perl(Memoize)

Requires: perl-libwww-perl
Requires: perl(JSON)
Requires: perl(JSON::XS)
Requires: perl(Memoize)
Requires: perl(LWP::Protocol::https)
Requires: perl(Data::Dump)
Requires: perl(File::ShareDir)
Requires: perl(Text::Unidecode)
Requires: perl(Unicode::LineBreak)
Requires: perl(Term::ReadLine::Gnu)
Requires: perl(Parallel::ForkManager)

Recommends: wget, mpv, yt-dlp, perl(Gtk3), perl(File::ShareDir)

%description
- A Lightweight application for searching and streaming videos from YouTube.

%prep
%setup -q

%build
%{__perl} Build.PL --destdir %{buildroot} --installdirs vendor --gtk

%check
./Build test

%install
./Build install --install_path script=/usr/bin
mkdir -p %{buildroot}/usr/share/{applications,pixmaps}
cp %{buildroot}/usr/share/perl5/vendor_perl/auto/share/dist/WWW-YoutubeViewer/gtk-youtube-viewer.desktop \
    %{buildroot}/usr/share/applications/gtk-youtube-viewer.desktop
cp %{buildroot}/usr/share/perl5/vendor_perl/auto/share/dist/WWW-YoutubeViewer/icons/gtk-youtube-viewer.png \
    %{buildroot}/usr/share/pixmaps/gtk-youtube-viewer.png
#rm -rf %{buildroot}/usr/{lib,lib64}

%files
%{_bindir}/*
%{_datadir}/*
%{_libdir}/*
