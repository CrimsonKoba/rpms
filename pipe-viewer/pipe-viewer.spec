Name:     pipe-viewer
Version:  0.5.0
Release:  1%{?dist}
Summary:  Application for searching and playing videos from YouTube.
Vendor:   Trizen <trizen@protonmail.com>

License: Artistic License 2.0
URL:     https://github.com/trizen/%{name}
Source:  %{url}/archive/%{version}.tar.gz

BuildArch: noarch
BuildRequires: perl(JSON)
BuildRequires: perl(Module::Build)
BuildRequires: perl(Test::More)
BuildRequires: perl(Memoize)

Requires: perl-libwww-perl
Requires: perl(JSON)
Requires: perl(Memoize)
Requires: perl(LWP::Protocol::https)
Requires: perl(Data::Dump)
Requires: perl(JSON::XS)
Requires: perl(Time::Piece)
Requires: perl(Term::ReadLine::Gnu)
Requires: perl(Unicode::LineBreak)
Requires: perl(Parallel::ForkManager)
# TODO
#Requires: perl(LWP::UserAgent::Cached)

Recommends: wget, mpv, ffmpeg, yt-dlp, perl(File::ShareDir), perl(Gtk3)

%description
- A lightweight application for searching and playing videos from YouTube.
- This parses the YouTube website directly and relies on the invidious instances only as a fallback method.

%prep
%setup -q

%build
%{__perl} Build.PL --gtk3

%check
./Build test

%install
./Build install --destdir %{buildroot} --installdirs vendor --install_path script=/usr/bin

mkdir -p %{buildroot}/usr/share/{applications,pixmaps}
cp %{buildroot}/usr/share/perl5/vendor_perl/auto/share/dist/WWW-PipeViewer/gtk-pipe-viewer.desktop \
    %{buildroot}/usr/share/applications/gtk-pipe-viewer.desktop
cp %{buildroot}/usr/share/perl5/vendor_perl/auto/share/dist/WWW-PipeViewer/icons/gtk-pipe-viewer.png \
    %{buildroot}/usr/share/pixmaps/gtk-pipe-viewer.png
#rm -rf %{buildroot}/usr/{lib,lib64}

%files
%{_bindir}/*
%{_datadir}/*
%{_libdir}/*
